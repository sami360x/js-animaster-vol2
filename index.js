(function main() {
    document.getElementById('fadeInPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('fadeInBlock');
            animaster().fadeIn(block, 5000);
        });
    document.getElementById('fadeOutPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('fadeOutBlock');
            animaster().fadeOut(block, 5000);
        });
    document.getElementById('showAndHidePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('showAndHideBlock');
            animaster().showAndHide(block, 5000);
        });
    document.getElementById('movePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('moveBlock');
            animaster().move(block, 1000, {
                x: 100,
                y: 10
            });
        });
    document.getElementById('scalePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('scaleBlock');
            animaster().scale(block, 1000, 1.25);
        });

    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function() {
            const block = document.getElementById('moveAndHideBlock');
            const moveAndHideAnimation = animaster().moveAndHide(block, 2000, {
                x: 100,
                y: 20
            });

            function forMoveAndHideAnimation() {
                moveAndHideAnimation.reset();
                document.getElementById('moveAndHideReset')
                    .removeEventListener('click', forMoveAndHideAnimation);
            }
            document.getElementById('moveAndHideReset')
                .addEventListener('click', forMoveAndHideAnimation);
        });





    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('heartBeatingBlock');
            const heartBeatingAnimation = animaster().heartBeating(block);
            function forHeartBeatingAnimation(){
                heartBeatingAnimation.stop();
                document.getElementById('heartBeatingStop')
                    .removeEventListener('click', forHeartBeatingAnimation);
            }
            document.getElementById('heartBeatingStop')
                .addEventListener('click', forHeartBeatingAnimation);

        });


    document.getElementById('customPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('customBlock');
            const anotherBlock = document.getElementById('customBlock1')
            const customAnimation = animaster()
                .addMove(200, {x: 40, y: 40})
                .addScale(800, 1.3)
                .addMove(200, {x: 80, y: 0})
                .addScale(800, 1)
                .addMove(200, {x: 40, y: -40})
                .addScale(800, 0.7)
                .addMove(200, {x: 0, y: 0})
                .addScale(800, 1);


            customAnimation.play(block);
            customAnimation.play(anotherBlock);

        });





    document.getElementById('shakingPlay')
        .addEventListener('click', function() {
            const block = document.getElementById('shakingBlock');
            const shakingPlayBlock = animaster().shaking(block, 1000, 1.25);
            function forShaking(){
                shakingPlayBlock.stop();
                document.getElementById('shakingStop')
                    .removeEventListener('click', forShaking);
            }
            document.getElementById('shakingStop')
                .addEventListener('click', forShaking);
        });
})();

function animaster() {
    function resetMoveAndScale(element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    }
    function resetFadeIn(element) {
        element.style.transitionDuration = null;
        element.classList.remove("show");
        element.classList.add("hide");
    }
    function resetFadeOut(element) {
        element.style.transitionDuration = null;
        element.classList.remove("hide");
        element.classList.add("show");
    }
    return {
        fadeIn: function(element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('hide');
            element.classList.add('show');
        },
        fadeOut: function(element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('show');
            element.classList.add('hide');
        },
        move: function(element, duration, translation) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(translation, null);
        },
        scale: function(element, duration, ratio) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(null, ratio);
        },
        moveAndHide: function(element, duration, translation) {
            let moveDuration = (duration / 5) * 2;
            let fadeOutDuration = duration - moveDuration;
            animaster().move(element, moveDuration, translation);
            let moveAndHide = setTimeout(animaster().fadeOut, moveDuration, element, fadeOutDuration)
            return {
                reset: function() {
                    clearTimeout(moveAndHide)
                    resetMoveAndScale(element)
                    resetFadeOut(element)
                }
            }
        },
        showAndHide: function(element, duration) {
            duration = duration / 3;
            animaster().fadeIn(element, duration);
            setTimeout(function() {
                animaster().fadeOut(element, duration);
            }, duration);
        },
        shaking: function(element) {
            let i = 1;
            function step() {
                animaster().move(element, 500, (i % 2 === 0) ? {
                    x: 20,
                    y: 0
                } : {
                    x: 0,
                    y: 0
                });
                i++;
            }
            const interval = setInterval(step, 1000);
            return {
                stop: function stop() {
                    clearInterval(interval);
                }
            }
        },
        heartBeating: function(element) {
            let stepCount = 1;
            function step() {
                animaster().scale(element, 500, stepCount % 2 ? 1 : 1.4);
                stepCount++
            }
            let heartBeatingTimer = setInterval(step, 1000);
            return {
                stop: function() {
                    clearInterval(heartBeatingTimer)
                }
            }
        },


        _steps: [],

        addMove: function(duration, translation) {
            this._steps.push({
                name: 'move',
                duration: duration,
                translation: translation
            })
            return this;
        },

        addScale: function addScale(duration, ratio) {
            this._steps.push({
                name: 'scale',
                duration: duration,
                ratio: ratio
            })
            return this;
        },

        addFadeIn: function addFadeIn(duration) {
            this._steps.push({
                name: 'fadeIn',
                duration: duration,
            })
            return this;
        },

        addFadeOut: function addFadeOut(duration) {
            this._steps.push({
                name: 'fadeOut',
                duration: duration,
            })
            return this;
        },
        play: function (element) {
            const animation = this._steps.shift()
            switch  (animation.name) {
                case "move":
                    animaster().move.call(this, element, animation.duration, animation.translation);
                    break;
                case "scale":
                    animaster().scale.call(this, element, animation.duration, animation.ratio);
                    break;
                case "fadeIn":
                    animaster().fadeIn.call(this, element, animation.duration);
                    break;
                case "fadeOut":
                    animaster().fadeOut.call(this, element,  animation.duration);
                    break;
            }
            if(this._steps.length !== 0) setTimeout(()=> this.play(element), animation.duration);
        },
    }
}

function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(' ');
}